﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Xml;
using System.Text;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

namespace DoReMeWebApplication
{
    public partial class Default : System.Web.UI.Page
    {
        string basePath = "";
        string DoReMeURL = "";
        protected void Page_Load(object sender, EventArgs e)
        {            
            try
            {
                DoReMeURL = String.Format("{0}/DoReMe/", Request.Url.AbsoluteUri.Replace("/Admin/Default.aspx", "").Replace("/admin/default.aspx", "").Replace("/default.aspx", "").Replace("/Default.aspx", ""));
                basePath = Server.MapPath(@"~\DoReMe\");

                if(!IsPostBack && !IsCallback)
                {
                    LoadUnits();
                }
                LoadLessons();
            }
            catch(Exception ex)
            {
                
            }
        }

        private void LoadUnits()
        {
            DataTable dt = new DataTable();
            DataRow dr;

            dt.Columns.Add(new DataColumn("ID", typeof(string)));
            dt.Columns.Add(new DataColumn("Field1", typeof(string)));

            string subDir;
            foreach (string subDirPath in Directory.GetDirectories(basePath, "*"))
            {
                subDir = subDirPath.Substring(subDirPath.LastIndexOf("\\") + 1);
                dr = dt.NewRow();
                dr[0] = subDir;
                dr[1] = subDir;
                dt.Rows.Add(dr);
            }
            cbUnits.DataSource = dt;
            cbUnits.DataBind();
        }

        private void LoadLessons()
        {
            
            string unitValue = cbUnits.Value == null ? "" : cbUnits.Value.ToString();

            DataTable dt = new DataTable();
            DataRow dr;

            dt.Columns.Add(new DataColumn("Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Subject", typeof(string)));
            dt.Columns.Add(new DataColumn("Type", typeof(string)));

            if (unitValue != "")
            {
                string name, subject, type;
                string subDirPath = basePath + unitValue;
                foreach (String f in Directory.GetFiles(subDirPath, "*", SearchOption.AllDirectories))
                {
                    name = f.Substring(f.LastIndexOf("\\") + 1, f.IndexOf(".") - f.LastIndexOf("\\") - 1);
                    subject = f.Substring(f.Remove(f.LastIndexOf("\\")).LastIndexOf("\\") + 1, f.LastIndexOf("\\") - f.Remove(f.LastIndexOf("\\")).LastIndexOf("\\") - 1);
                    type = f.Substring(f.LastIndexOf(".") + 1);
                    dr = dt.NewRow();
                    dr[0] = name;
                    dr[1] = subject;
                    dr[2] = type;
                    dt.Rows.Add(dr);
                }
            }
            gvLessons.DataSource = dt;
            gvLessons.DataBind();
        }

        protected void cbUnits_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
        {
            try
            {
                cbUnits.JSProperties["cpErrorMessage"] = "";
                cbUnits.JSProperties["cpException"] = "";

                string strValue = cbUnits.Value == null ? "" : cbUnits.Value.ToString();

                if(e.Parameter=="ADD")
                {
                    //insert Validation Here  : '
                    if (!fileNameValid(tbUnit.Text))
                        cbUnits.JSProperties["cpErrorMessage"] = "Not a Valid Unit Name";

                    foreach (string dir in Directory.GetDirectories(basePath))
                        if (dir.ToLower() == basePath.ToLower() + tbUnit.Text.ToLower())                        
                            cbUnits.JSProperties["cpErrorMessage"] = "This Unit already exists";
                        
                    

                    if (cbUnits.JSProperties["cpErrorMessage"].ToString() == "")
                    {
                        string newPath = String.Format("{0}{1}", basePath, tbUnit.Text);
                        Directory.CreateDirectory(newPath);
                        Directory.CreateDirectory(newPath + "\\TeacherHomework");
                        Directory.CreateDirectory(newPath + "\\CD_Listening");
                        Directory.CreateDirectory(newPath + "\\Reading");
                        Directory.CreateDirectory(newPath + "\\Movement");
                        Directory.CreateDirectory(newPath + "\\Instruments");

                        strValue = tbUnit.Text;//set combobox to new value
                    }                    
                }
                else if (e.Parameter.Split(':')[0] == "EDIT")
                {
                    //insert Validation Here  : '
                    if (!fileNameValid(tbUnit.Text))
                        cbUnits.JSProperties["cpErrorMessage"] = "Not a Valid Unit Name";
                    foreach (string dir in Directory.GetDirectories(basePath))
                        if (dir.ToLower() == basePath.ToLower() + tbUnit.Text.ToLower())
                            cbUnits.JSProperties["cpErrorMessage"] = "This Unit already exists";
                    
                    string newPath = String.Format("{0}{1}", basePath, tbUnit.Text);
                    string oldPath = String.Format("{0}{1}", basePath, e.Parameter.Split(':')[1]);
                    if (cbUnits.JSProperties["cpErrorMessage"].ToString() == "")
                    {
                        //rename folder
                        CopyFolder(oldPath, newPath);
                        Directory.Delete(oldPath, true);

                        strValue = tbUnit.Text;//set combobox to new value
                        updateXML();
                    }
                }
                else if (e.Parameter.Split(':')[0] == "REMOVE")
                {
                    string strOldValue = e.Parameter.Split(':')[1];
                    string oldPath = basePath + strOldValue;
                    Directory.Delete(oldPath, true);

                    strValue = "";//set combobox to nothing
                    updateXML();
                }


                LoadUnits();
                if (strValue != "")
                    cbUnits.Value = strValue;
            }
            catch(Exception ex)
            {
                cbUnits.JSProperties["cpErrorMessage"] = "There was an error. Please try again.";
                cbUnits.JSProperties["cpException"] = ex.Message;
            }

        }
        private void CopyFolder(string sourceFolder, string destFolder)
        {
            if (!Directory.Exists(destFolder))
                Directory.CreateDirectory(destFolder);
            string[] files = Directory.GetFiles(sourceFolder);
            foreach (string file in files)
            {
                string name = Path.GetFileName(file);
                string dest = Path.Combine(destFolder, name);
                File.Copy(file, dest);
            }
            string[] folders = Directory.GetDirectories(sourceFolder);
            foreach (string folder in folders)
            {
                string name = Path.GetFileName(folder);
                string dest = Path.Combine(destFolder, name);
                CopyFolder(folder, dest);
            }
        }


        private void updateXML()
        {
            
            XmlTextWriter xmlwriter = new XmlTextWriter(basePath + "\\Index.xml", Encoding.UTF8);
            xmlwriter.Formatting = Formatting.Indented;
            xmlwriter.WriteStartDocument();
            xmlwriter.WriteStartElement("DoReMe");
            string unit, subject, file;
            foreach (string subDirPath in Directory.GetDirectories(basePath))
            {
                unit = subDirPath.Substring(subDirPath.LastIndexOf("\\") + 1);
                xmlwriter.WriteStartElement("Unit");
                //xmlwriter.WriteAttributeString("Name", unit.Substring(5));//only get the number
                xmlwriter.WriteAttributeString("Name", unit);
                foreach (string subjectPath in Directory.GetDirectories(subDirPath))
                {
                    subject = subjectPath.Substring(subjectPath.LastIndexOf("\\") + 1);
                    xmlwriter.WriteStartElement("Subject");
                    xmlwriter.WriteAttributeString("Name", subject);
                    DirectoryInfo dir = new DirectoryInfo(subjectPath);
                    foreach (FileInfo fileInfo in dir.GetFiles())
                    {
                        file = fileInfo.Name;
                        xmlwriter.WriteStartElement("Lesson");
                        xmlwriter.WriteAttributeString("name", file.Substring(0, file.LastIndexOf(".")));//take off the extension
                        string path = DoReMeURL + unit + "/" + subject + "/" + file;
                        xmlwriter.WriteAttributeString("path", path);
                        xmlwriter.WriteEndElement();//Lesson
                    }
                    xmlwriter.WriteEndElement();//Subject
                }
                xmlwriter.WriteEndElement();//Unit
            }
            xmlwriter.WriteEndElement();//DoReMe
            xmlwriter.WriteEndDocument();
            xmlwriter.Flush();
            xmlwriter.Close();

        }

        protected void gvLessons_CustomCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
        {
            gvLessons.JSProperties["cpErrorMessage"] = "";
            gvLessons.JSProperties["cpException"] = "";
            try
            {
                if (e.Parameters.Split(':')[0] == "EDIT" && e.Parameters.Split(':')[4] == "TRUE")
                {
                    if (!fileNameValid(tbLessonName.Text))
                        gvLessons.JSProperties["cpErrorMessage"] = "Not a Valid Lesson Name";

                    try
                    {
                        if (new System.IO.FileInfo(String.Format(@"{0}{1}\{2}\{3}.{4}", basePath, cbUnits.Text, cbSubject.Text, tbLessonName.Text, e.Parameters.Split(':')[3])).Exists)
                            gvLessons.JSProperties["cpErrorMessage"] = "Lesson Name Already Exists";
                    }
                    catch {
                        gvLessons.JSProperties["cpErrorMessage"] = "A Path Error Occured";
                    }


                    if (gvLessons.JSProperties["cpErrorMessage"].ToString() == "")
                    {
                        string src = String.Format(@"{0}{1}\{2}\{3}.{4}", basePath, cbUnits.Text, e.Parameters.Split(':')[1], e.Parameters.Split(':')[2], e.Parameters.Split(':')[3]);
                        string dest = String.Format(@"{0}{1}\{2}\{3}.{4}", basePath, cbUnits.Text, cbSubject.Text, tbLessonName.Text, e.Parameters.Split(':')[3]);
                        File.Move(src, dest);
                    }
                }
                else if (e.Parameters.Split(':')[0] == "REMOVE")
                {
                    File.Delete(String.Format(@"{0}{1}\{2}\{3}.{4}" , basePath, cbUnits.Text,  e.Parameters.Split(':')[1],  e.Parameters.Split(':')[2],  e.Parameters.Split(':')[3]));
                   
                }

                updateXML();
                LoadLessons();
            }
            catch(Exception ex)
            {
                gvLessons.JSProperties["cpErrorMessage"] = "There was an error. Please try again.";
                gvLessons.JSProperties["cpException"] = ex.Message;
            }
            
        }

        protected void ucLesson_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
        {
            ucLesson.JSProperties["cpErrorMessage"] = "";
            ucLesson.JSProperties["cpException"] = "";
            try
            {
                if (e.UploadedFile.IsValid)
                {
                    if (HiddenFieldPendingFunction.Value.Split(':')[0].Contains("ADD"))
                    {
                        if (!fileNameValid(tbLessonName.Text))
                            ucLesson.JSProperties["cpErrorMessage"] = "Not a Valid Lesson Name";

                        if (ucLesson.JSProperties["cpErrorMessage"].ToString() == "")
                        {
                            FileInfo fileInfo = new FileInfo(e.UploadedFile.FileName);
                            e.UploadedFile.SaveAs(String.Format(@"{0}{1}\{2}\{3}{4}", basePath, cbUnits.Text, cbSubject.Text, tbLessonName.Text, fileInfo.Extension));
                        }
                    }
                    else if (HiddenFieldPendingFunction.Value.Split(':')[0].Contains("EDIT"))
                    {
                        //TODO make sure new old does not exist or = to old one
                        //put in validation no : ' "
                        if (!fileNameValid(tbLessonName.Text))
                            ucLesson.JSProperties["cpErrorMessage"] = "Not a Valid Lesson Name";

                        try
                        {
                            FileInfo fileInfo = new FileInfo(e.UploadedFile.FileName);
                            if (String.Format(@"{0}{1}\{2}\{3}.{4}", basePath, cbUnits.Text, HiddenFieldPendingFunction.Value.Split(':')[1], HiddenFieldPendingFunction.Value.Split(':')[2], HiddenFieldPendingFunction.Value.Split(':')[3]) != String.Format(@"{0}{1}\{2}\{3}{4}", basePath, cbUnits.Text, cbSubject.Text, tbLessonName.Text, fileInfo.Extension) && new System.IO.FileInfo(String.Format(@"{0}{1}\{2}\{3}{4}", basePath, cbUnits.Text, cbSubject.Text, tbLessonName.Text, fileInfo.Extension)).Exists)
                                ucLesson.JSProperties["cpErrorMessage"] = "Lesson Name Already Exists";
                        }
                        catch
                        {
                            ucLesson.JSProperties["cpErrorMessage"] = "A Path Error Occured";
                        }

                        if (ucLesson.JSProperties["cpErrorMessage"].ToString() == "")
                        {
                            File.Delete(String.Format(@"{0}{1}\{2}\{3}.{4}", basePath, cbUnits.Text, HiddenFieldPendingFunction.Value.Split(':')[1], HiddenFieldPendingFunction.Value.Split(':')[2], HiddenFieldPendingFunction.Value.Split(':')[3]));

                            //make new
                            FileInfo fileInfo = new FileInfo(e.UploadedFile.FileName);
                            e.UploadedFile.SaveAs(String.Format(@"{0}{1}\{2}\{3}{4}", basePath, cbUnits.Text, cbSubject.Text, tbLessonName.Text, fileInfo.Extension));
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                cbUnits.JSProperties["cpErrorMessage"] = "There was an error. Please try again.";
                cbUnits.JSProperties["cpException"] = ex.Message;
            }
        }

        protected void gvLessons_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            try
            {
                if (e.RowType == DevExpress.Web.ASPxGridView.GridViewRowType.Data)
                {

                    string strUnit = cbUnits.Text;
                    string strSubject = (sender as ASPxGridView).GetRowValues(e.VisibleIndex, "Subject").ToString();
                    string strLesson = (sender as ASPxGridView).GetRowValues(e.VisibleIndex, "Name").ToString();
                    string strType = (sender as ASPxGridView).GetRowValues(e.VisibleIndex, "Type").ToString();
                    ASPxButton mbtnLessonDownload = (sender as ASPxGridView).FindRowCellTemplateControl(e.VisibleIndex, null, "btnLessonDownload") as ASPxButton;
                    mbtnLessonDownload.ClientSideEvents.Click = String.Format("function (s, e){{ window.open('{0}/{1}/{2}/{3}.{4}');  }}", DoReMeURL, strUnit, strSubject, strLesson, strType);
                    ASPxButton mbtnLessonRemove = (sender as ASPxGridView).FindRowCellTemplateControl(e.VisibleIndex, null, "btnLessonRemove") as ASPxButton;
                    mbtnLessonRemove.ClientSideEvents.Click = String.Format(@"function (s, e){{ DeleteLesson('{0}', '{1}', '{2}');  }}", strSubject, strLesson, strType);
                    ASPxButton mbtnLessonEdit = (sender as ASPxGridView).FindRowCellTemplateControl(e.VisibleIndex, null, "btnLessonEdit") as ASPxButton;
                    mbtnLessonEdit.ClientSideEvents.Click = String.Format(@"function (s, e){{ EditLesson('{0}', '{1}', '{2}');  }}", strSubject, strLesson, strType);
                }
            }
            catch
            {

            }
        }
        private bool fileNameValid(String s)
        {
            if (s.IndexOfAny(Path.GetInvalidFileNameChars()) != -1||s.Contains(":")||s.Contains("'")||s.Contains("\"")||s.Contains("/"))
                return false;
            return true;
        }
    }
}