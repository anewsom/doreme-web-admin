﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DoReMeWebApplication.Default" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGlobalEvents" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Administrator Console for the DoReMe iOS App</title>
    <style type="text/css">
        body {
            /*background-color: #01305A;
            color: #707171;*/
        }

        .center {
            margin: 0px auto;
        }

        .left {
            float: left;
        }

        .right {
            float: right;
        }

        .bold {
            font-weight: bold;
        }

        /* http://sonspring.com/journal/clearing-floats */
        .clear {
            clear: both;
            display: block;
            overflow: hidden;
            visibility: hidden;
            width: 0;
            height: 0;
        }
        /* http://perishablepress.com/press/2008/02/05/lessons-learned-concerning-the-clearfix-css-hack */
        .clearfix:after {
            clear: both;
            content: ' ';
            display: block;
            font-size: 0;
            line-height: 0;
            visibility: hidden;
            width: 0;
            height: 0;
        }

        .clearfix {
            display: inline-block;
        }

        * html .clearfix {
            height: 1%;
        }

        .clearfix {
            display: block;
        }


        .control-left {
            float: left;
            margin-right: 10px;
            margin-bottom: 10px;
        }

        .control-text {            
            margin-top: 10px;
            width:60px;
        }

        .control-save {
            float: right;
            margin-left: 10px;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .lessons {            
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <script type="text/javascript">
                function AddUnit()
                {
                    pcUnits.Show();
                    ASPxClientEdit.ClearGroup('submitEditUnitGroup');
                    document.getElementById('HiddenFieldPendingFunction').value = "cbUnits.PerformCallback('ADD');";
                }
                function EditUnit()
                {
                    if (ASPxClientEdit.ValidateGroup('submitUnitGroup')) {
                        pcUnits.Show();
                        document.getElementById('HiddenFieldPendingFunction').value = "cbUnits.PerformCallback('EDIT:" + cbUnits.GetText() + "');";
                        tbUnit.SetText(cbUnits.GetText());
                    }
                }
                function RemoveUnit()
                {
                    if(ASPxClientEdit.ValidateGroup('submitUnitGroup'))     
                    {                                    
                        document.getElementById('HiddenFieldPendingFunction').value = "cbUnits.PerformCallback('REMOVE:" + cbUnits.GetText() + "');";
                        lblConfirm.SetText('Are you sure you want to remove this unit?');
                        pcConfirm.Show();
                    }
                }
                function AddLesson()
                {
                    if (ASPxClientEdit.ValidateGroup('submitUnitGroup')) {
                        pcLesson.Show();
                        document.getElementById('HiddenFieldPendingFunction').value = "gvLessons.PerformCallback('ADD";
                        lblUnit.SetText(cbUnits.GetText());
                        ASPxClientEdit.ClearGroup('submitEditLessonGroup');
                    }
                }
                function DeleteLesson(Subject, Lesson, Type) {
                    document.getElementById('HiddenFieldPendingFunction').value = "gvLessons.PerformCallback('REMOVE:" + Subject + ":" + Lesson + ":" + Type + "')";
                    lblConfirm.SetText('Are you sure you want to remove this lesson?');
                    pcConfirm.Show();
                }
                function EditLesson(Subject, Lesson, Type) {
                    document.getElementById('HiddenFieldPendingFunction').value = "gvLessons.PerformCallback('EDIT:" + Subject + ":" + Lesson + ":" + Type;
                    pcLesson.Show();
                    lblUnit.SetText(cbUnits.GetText());
                    tbLessonName.SetText(Lesson);
                    cbSubject.SetText(Subject);
                }

                function UploadLesson(ReplaceFile)
                {
                    if (ReplaceFile) {
                        lblLessonError.SetText('');
                        if (ASPxClientEdit.ValidateGroup('submitEditLessonGroup')) {
                            if (document.getElementById('HiddenFieldPendingFunction').value == "gvLessons.PerformCallback('ADD")
                                ucLesson.Upload();
                            else if (ucLesson.GetText() != '') {
                                ucLesson.Upload();
                            }
                            else {
                                var s = document.getElementById('HiddenFieldPendingFunction').value + ":TRUE');";
                                eval(s);
                            }


                        }
                    }
                    else
                    {
                        if(ucLesson.cpException!='')
                        {
                            alert(cbUnits.cpException);
                        }
                        if(ucLesson.cpErrorMessage!='')
                        {
                            lblLessonError.SetText(cbUnits.cpErrorMessage);                                    
                        }
                        else
                        {
                            var s = document.getElementById('HiddenFieldPendingFunction').value + ":FALSE');";
                            eval(s);
                        } 
                    }
                }
            </script>

            <asp:HiddenField ID="HiddenFieldPendingFunction" runat="server" />

            <dx:ASPxPopupControl ID="pcConfirm" ClientInstanceName="pcConfirm" HeaderText="Please Confirm:" runat="server" Width="450px" Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
                <ContentCollection>
                    <dx:PopupControlContentControl runat="server">
                        <dx:ASPxLabel ID="lblConfirm" ClientInstanceName="lblConfirm" runat="server" Text="" EncodeHtml="false"></dx:ASPxLabel>
                        <div class="clear"></div>
                                                
                        <dx:ASPxButton ID="btnConfirmNo" runat="server" Text="NO" CssClass="control-save" AutoPostBack="false">
                            <ClientSideEvents Click="function(s, e){  
                                pcConfirm.Hide();                                                                                          
                                  
                            }" />
                        </dx:ASPxButton>
                        <dx:ASPxButton ID="btnConfirmYes" runat="server" Text="YES" CssClass="control-save" AutoPostBack="false">
                            <ClientSideEvents Click="function(s, e){                                                            
                                    pcConfirm.Hide();
                                  eval(document.getElementById('HiddenFieldPendingFunction').value);
                            }" />
                        </dx:ASPxButton>
                        <div class="clear"></div>
                        <dx:ASPxLabel ID="ASPxLabel1" ClientInstanceName="lblUnitError" runat="server" Text="" CssClass="center"></dx:ASPxLabel>
                    </dx:PopupControlContentControl>
                </ContentCollection>
            </dx:ASPxPopupControl>

            <dx:ASPxPopupControl ID="pcUnits" ClientInstanceName="pcUnits" HeaderText="Grade:" runat="server" Width="450px" Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
                <ClientSideEvents Shown="function(s, e){
                    lblUnitError.SetText('');
                }" />
                <ContentCollection>
                    <dx:PopupControlContentControl runat="server">
                        <div class="control-left control-text bold">Grade: </div>
                        <dx:ASPxTextBox ID="tbUnit" ClientInstanceName="tbUnit" runat="server" Width="270px" CssClass="left">
                            <ValidationSettings ValidationGroup="submitEditUnitGroup" ErrorDisplayMode="ImageWithTooltip">
                                <RequiredField IsRequired="true" />
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                        <div class="clear"></div>
                        
                        <dx:ASPxButton ID="btnUnitSave" runat="server" Text="SAVE" CssClass="control-save" AutoPostBack="false">
                            <ClientSideEvents Click="function(s, e){
                                lblUnitError.SetText('');
                                if(ASPxClientEdit.ValidateGroup('submitEditUnitGroup'))                                
                                    eval(document.getElementById('HiddenFieldPendingFunction').value);
                                  
                            }" />
                        </dx:ASPxButton>
                        <div class="clear"></div>
                        <dx:ASPxLabel ID="lblUnitError" ClientInstanceName="lblUnitError" runat="server" Text="" CssClass="center" ForeColor="Red"></dx:ASPxLabel>
                    </dx:PopupControlContentControl>
                </ContentCollection>
            </dx:ASPxPopupControl>

            
            <dx:ASPxPopupControl ID="pcLesson" ClientInstanceName="pcLesson" HeaderText="Lesson:" runat="server" Width="450px" Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
                <ClientSideEvents Shown="function(s, e){
                    lblLessonError.SetText('');
                }" />
                <ContentCollection>
                    <dx:PopupControlContentControl runat="server">
                        <div class="control-left control-text bold">Grade: </div>
                        <div class="control-left control-text">
                        <dx:ASPxLabel ID="lblUnit" ClientInstanceName="lblUnit" runat="server" Width="270px" Text="">                            
                        </dx:ASPxLabel>
                            </div>
                         <div class="clear"></div>
                        <div class="control-left control-text bold">Name: </div>
                        <dx:ASPxTextBox ID="tbLessonName" ClientInstanceName="tbLessonName" runat="server" Width="270px" CssClass="control-left">
                            <ValidationSettings ValidationGroup="submitEditLessonGroup" ErrorDisplayMode="ImageWithTooltip">
                                <RequiredField IsRequired="true" />
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                        <div class="clear"></div>
                        <div class="control-left control-text bold">Subject:</div>
                        <dx:ASPxComboBox ID="cbSubject" ClientInstanceName="cbSubject" runat="server" Width="270px" CssClass="control-left">
                            <ValidationSettings ValidationGroup="submitEditLessonGroup" ErrorDisplayMode="ImageWithTooltip">
                                <RequiredField IsRequired="true" />
                            </ValidationSettings>
                            <Items>
                                <dx1:ListEditItem Text="CD_Listening" Value="CD_Listening" />
                                <dx1:ListEditItem Text="Instruments" Value="Instruments" />
                                <dx1:ListEditItem Text="Movement" Value="Movement" />
                                <dx1:ListEditItem Text="Reading" Value="Reading" />
                                <dx1:ListEditItem Text="TeacherHomework" Value="TeacherHomework" />
                            </Items>
                        </dx:ASPxComboBox>
                        <div class="clear"></div>
                        <div class="control-left control-text bold">File: </div>
                        <dx:ASPxUploadControl ID="ucLesson" ClientInstanceName="ucLesson" runat="server" UploadMode="Auto" Width="270px" OnFileUploadComplete="ucLesson_FileUploadComplete" FileUploadMode="OnPageLoad">
                            <ValidationSettings MaxFileSize="200000000">                                            
                            </ValidationSettings>
                            <ClientSideEvents FilesUploadComplete="function (s, e) {                                
                                 UploadLesson(false);                                                                                                                                                 
                            }" />
                        </dx:ASPxUploadControl>
                        <div class="clear"></div>
                        
                        <dx:ASPxButton ID="btnLessonSave" runat="server" Text="SAVE" CssClass="control-save" AutoPostBack="false">
                            <ClientSideEvents Click="function(s, e){
                                   UploadLesson(true);                       
                                    
                                  
                            }" />
                        </dx:ASPxButton>
                        <div class="clear"></div>
                        <dx:ASPxLabel ID="lblLessonError" ClientInstanceName="lblLessonError" runat="server" Text="" CssClass="center" ForeColor="Red"></dx:ASPxLabel>
                    </dx:PopupControlContentControl>
                </ContentCollection>
            </dx:ASPxPopupControl>

            <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="800px" CssClass="center" HeaderText="Administrator Console for the DoReMe iOS App">
                <PanelCollection>
                    <dx:PanelContent runat="server">
                         <div class="control-left control-text bold">Grades:</div>
                        <dx:ASPxComboBox ID="cbUnits" ClientInstanceName="cbUnits" runat="server" ValueType="System.String" CssClass="control-left" Width="300px" TextField="Field1" ValueField="ID" OnCallback="cbUnits_Callback" EnableViewState="False" >
                            <ValidationSettings ValidationGroup="submitUnitGroup" ErrorDisplayMode="ImageWithTooltip" Display="None">
                                <RequiredField IsRequired="true" />
                            </ValidationSettings>
                            <ClientSideEvents EndCallback="function(s, e){
                                if(cbUnits.cpException!='')
                                {
                                    alert(cbUnits.cpException);
                                }
                                if(cbUnits.cpErrorMessage!='')
                                {
                                    lblUnitError.SetText(cbUnits.cpErrorMessage); 
                                                                      
                                }
                                else
                                {
                                    pcUnits.Hide();
                                }
                                gvLessons.Refresh(); 
                            }" SelectedIndexChanged="function(s, e){
                                gvLessons.Refresh();
                                }" />
                        </dx:ASPxComboBox>
                        <dx:ASPxButton ID="btnAddUnit" runat="server" Text="" CssClass="control-left" AutoPostBack="false">
                            <Image Url="~/Images/Add.png">
                            </Image>
                            <ClientSideEvents Click="function(s, e){
                                AddUnit();
                                }" />
                        </dx:ASPxButton>
                        <dx:ASPxButton ID="btnEditUnit" runat="server" Text="" CssClass="control-left" AutoPostBack="false">
                            <Image Url="~/Images/Edit.png">
                            </Image>
                            <ClientSideEvents Click="function(s, e){
                                EditUnit();
                                }" />
                        </dx:ASPxButton>
                        <dx:ASPxButton ID="btnRemoveUnit" runat="server" Text="" CssClass="left" AutoPostBack="false">
                            <Image Url="~/Images/Remove.png">
                            </Image>
                            <ClientSideEvents Click="function(s, e){
                                RemoveUnit();
                                }" />
                        </dx:ASPxButton>
                        
                        <div class="clear"></div>
                        <div class="lessons">
                            <div class="control-text left bold">Lessons:</div>
                             <dx:ASPxButton ID="btnAddLesson" runat="server" Text="" CssClass="control-save" AutoPostBack="false">
                                <Image Url="~/Images/Add.png">
                                </Image>
                                <ClientSideEvents Click="function(s, e){
                                    AddLesson();
                                    }" />
                            </dx:ASPxButton>
                            <div class="clear"></div>
                            <dx:ASPxGridView ID="gvLessons" ClientInstanceName="gvLessons" runat="server" Width="100%" OnCustomCallback="gvLessons_CustomCallback" AutoGenerateColumns="False" OnHtmlRowCreated="gvLessons_HtmlRowCreated">
                                <ClientSideEvents EndCallback="function(s, e){
                                if(gvLessons.cpException!='' && gvLessons.cpException != undefined)
                                {
                                    alert(gvLessons.cpException);
                                }
                                if(gvLessons.cpErrorMessage!='' && gvLessons.cpErrorMessage != undefined)
                                {
                                    lblLessonError.SetText(gvLessons.cpErrorMessage);                                    
                                }
                                else
                                    pcLesson.Hide();
                            }"  />
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="Name" ShowInCustomizationForm="True" VisibleIndex="0">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Subject" ShowInCustomizationForm="True" VisibleIndex="1" Width="150px">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Type" ShowInCustomizationForm="True" VisibleIndex="2" Width="75px">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn FieldName="Edit" Caption=" " VisibleIndex="3" UnboundType="String" Width="250px">                                    
                                        <DataItemTemplate>
                                                <dx:ASPxButton ID="btnLessonDownload" runat="server"  Image-Url="~/Images/Download.png" Text="" CssClass="control-left" UseSubmitBehavior="false" AutoPostBack="false">                                            
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btnLessonEdit" runat="server"  Image-Url="~/Images/Edit.png" Text="" CssClass="control-left" UseSubmitBehavior="false" AutoPostBack="false">                                            
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btnLessonRemove" runat="server"  Image-Url="~/Images/Remove.png" Text="" CssClass="control-left" UseSubmitBehavior="false" AutoPostBack="false">                                            
                                                </dx:ASPxButton>

                                        </DataItemTemplate>
                                    </dx:GridViewDataColumn> 
                                                                        
                                </Columns>
                                 <SettingsPager Mode="ShowAllRecords">
                                </SettingsPager>
                                <SettingsBehavior AllowDragDrop="False" AllowSort="False"  />
                            </dx:ASPxGridView>
                        </div>
                        <div class="lessons">
                            If you have any questions or issues, contact Aaron Newsom at <a href="mailto:newsomaj@stoneenergy.com">newsomaj@stoneenergy.com</a>
                        </div>
                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxRoundPanel>

            <dx:ASPxGlobalEvents ID="ASPxGlobalEvents1" runat="server">
                <clientsideevents ControlsInitialized ="function(s,e){ 
                if(!e.isCallback){                    
                        //pcUnits.Show();
                }                
            }" />
            </dx:ASPxGlobalEvents>
        </div>
    </form>
</body>
</html>
